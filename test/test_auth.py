from typing import Any
from unittest import mock

from flask_login import LoginManager
from flask_testing import TestCase

from src import db, create_app
from src.blueprints.auth import INCORRECT_PASSWORD, EMAIL_DOES_NOT_EXIST
from src.ds.models import User
from src.forms.general_forms import EMAIL_EXISTS, INVALID_EMAIL, PASSWORDS_MUST_MATCH
from src.models import web_models
from src.models.general_models import FlaskSetting
from test import samples
from test.adapters.fake_current_user import FakeCurrentUser
from test.adapters.fake_listing_ads import FakeListingAdsAdapter
from test.adapters.fake_user_listings import FakeUserListingsAdapter
from test.adapters.fake_users import FakeUsersAdapter

CLIENT_CONFIG = {"follow_redirects": False}


login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class AuthTest(TestCase):
    @mock.patch("src.DB_NAME", ":memory:")
    def create_app(self):
        app = create_app()
        app.config[FlaskSetting.TESTING] = True
        app.config[FlaskSetting.WTF_CSRF_ENABLED] = False
        app.config[FlaskSetting.LOGIN_DISABLED] = True

        return app

    def setUp(self):
        self.patch("src.blueprints.auth.users", new=FakeUsersAdapter())
        self.patch("src.blueprints.auth.user_listings", new=FakeUserListingsAdapter())
        self.patch("src.blueprints.auth.listing_ads", new=FakeListingAdsAdapter())
        self.patch("src.blueprints.auth.current_user", new=FakeCurrentUser())
        self.patch("src.blueprints.auth.login_user")
        self.patch("src.blueprints.auth.logout_user")
        db.create_all()
        self.authenticated_client()

    def patch(self, path: str, new: Any = mock.MagicMock()):
        patcher = mock.patch(path, new)
        patcher.start()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def authenticated_client(self):
        login_manager.init_app(self.app)
        login_manager.user_loader(load_user)
        with self.client:
            yield self.client

    def test_login(self):
        with mock.patch("src.blueprints.auth.check_password_hash", return_value=True):
            response = self.client.post(
                web_models.WebPath.login.endpoint,
                data={**samples.LOGIN_FORM_DATA},
                **CLIENT_CONFIG
            )
            self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_login_unsuccessful(self):
        with mock.patch("src.blueprints.auth.check_password_hash", return_value=False):
            response = self.client.post(
                web_models.WebPath.login.endpoint,
                data={**samples.LOGIN_FORM_DATA},
                **CLIENT_CONFIG
            )
            self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
            self.assertIn(INCORRECT_PASSWORD.encode(), response.data)

    def test_login_unexisting(self):
        form_payload = {**samples.LOGIN_FORM_DATA}
        form_payload["email"] = samples.EMAIL_2
        response = self.client.post(
            web_models.WebPath.login.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(EMAIL_DOES_NOT_EXIST.encode(), response.data)

    def test_login_get(self):
        response = self.client.get(web_models.WebPath.login.endpoint, **CLIENT_CONFIG)
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_logout(self):
        response = self.client.get(web_models.WebPath.logout.endpoint, **CLIENT_CONFIG)
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_sign_up(self):
        response = self.client.post(
            web_models.WebPath.sign_up.endpoint,
            data={**samples.SIGNUP_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_sign_up_email_exists(self):
        form_payload = {**samples.SIGNUP_FORM_DATA}
        form_payload["email"] = samples.EMAIL
        response = self.client.post(
            web_models.WebPath.sign_up.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(EMAIL_EXISTS.encode(), response.data)

    def test_sign_up_invalid_email(self):
        form_payload = {**samples.SIGNUP_FORM_DATA}
        form_payload["email"] = "testexample.com"
        response = self.client.post(
            web_models.WebPath.sign_up.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(INVALID_EMAIL.encode(), response.data)

    def test_sign_up_passwords_dont_match(self):
        form_payload = {**samples.SIGNUP_FORM_DATA}
        form_payload["password2"] = "testpasswor"
        response = self.client.post(
            web_models.WebPath.sign_up.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(PASSWORDS_MUST_MATCH.encode(), response.data)

    def test_sign_up_password_too_short(self):
        form_payload = {**samples.SIGNUP_FORM_DATA}
        form_payload["password1"] = "test"
        form_payload["password2"] = "test"
        response = self.client.post(
            web_models.WebPath.sign_up.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(b"Field must be between 8 and 35 characters long.", response.data)

    def test_sign_up_get(self):
        response = self.client.get(web_models.WebPath.sign_up.endpoint, **CLIENT_CONFIG)
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_edit_profile(self):
        form_payload = {**samples.LOGIN_FORM_DATA}
        form_payload["email"] = samples.EMAIL_2
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_edit_profile_email_exists(self):
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint,
            data={**samples.LOGIN_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(EMAIL_EXISTS.encode(), response.data)

    def test_edit_profile_incorrect_password(self):
        form_payload = {**samples.LOGIN_FORM_DATA}
        form_payload["email"] = samples.EMAIL_2
        form_payload["password"] = "testpasswor"
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(INCORRECT_PASSWORD.encode(), response.data)

    def test_change_password(self):
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint,
            data={**samples.CHANGE_PASS_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_change_password_dont_match(self):
        form_payload = {**samples.CHANGE_PASS_FORM_DATA}
        form_payload["password2"] = "testpasswor"
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(PASSWORDS_MUST_MATCH.encode(), response.data)

    def test_change_password_incorrect(self):
        form_payload = {**samples.CHANGE_PASS_FORM_DATA}
        form_payload["old_password"] = "testpasswor"
        response = self.client.post("/edit-profile", data=form_payload, **CLIENT_CONFIG)
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(INCORRECT_PASSWORD.encode(), response.data)

    def test_change_password_too_short(self):
        form_payload = {**samples.CHANGE_PASS_FORM_DATA}
        form_payload["password1"] = "new"
        form_payload["password2"] = "new"
        response = self.client.post(
            web_models.WebPath.edit_profile.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(b"Field must be between 8 and 35 characters long.", response.data)

    def test_change_password_get(self):
        response = self.client.get(
            web_models.WebPath.edit_profile.endpoint, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_delete_profile(self):
        form_payload = {}
        with mock.patch("src.blueprints.auth.db"):
            response = self.client.post(
                web_models.WebPath.delete_profile.endpoint,
                data=form_payload,
                **CLIENT_CONFIG
            )
            self.assertEqual(response.status_code, web_models.HTTPResponseCode.REDIRECT)

    def test_delete_profile_get(self):
        response = self.client.get(
            web_models.WebPath.delete_profile.endpoint, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
