from unittest import mock

from flask_testing import TestCase
from werkzeug.datastructures import ImmutableMultiDict
from wtforms.fields import choices, simple

from src import db, create_app
from src.ads_manager import AdsManager, LimitReached, ValidationError
from src.forms.custom_listings import NoFiltersFound
from src.forms.general_forms import NewURLForm
from src.models import general_models, web_models
from test import samples
from test.adapters.fake_current_user import FakeCurrentUser
from test.adapters.fake_listing_ads import FakeListingAdsAdapter
from test.adapters.fake_scraper import FakeScraperFactory
from test.adapters.fake_user_listings import FakeUserListingsAdapter
from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME


class FlaskTest(TestCase):
    @mock.patch("src.DB_NAME", ":memory:")
    def create_app(self):
        app = create_app()
        app.config[general_models.FlaskSetting.TESTING] = True
        app.config[general_models.FlaskSetting.WTF_CSRF_ENABLED] = False
        app.config[general_models.FlaskSetting.WTF_CSRF_METHODS] = []
        app.config[general_models.FlaskSetting.LOGIN_DISABLED] = True

        return app

    def setUp(self, *args, **kwargs):
        db.create_all()
        self.scraper_factory_mock = FakeScraperFactory()
        self.fake_user_listings = FakeUserListingsAdapter()
        self.fake_listing_ads = FakeListingAdsAdapter()
        self.fake_current_user = FakeCurrentUser()

        self.ads_manager = AdsManager(
            self.fake_current_user,
            self.scraper_factory_mock,
            self.fake_user_listings,
            self.fake_listing_ads,
        )

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_get_user_listings(self):
        result = self.ads_manager.get_user_listings()
        expected_result = [
            f"{samples.LABEL_1_LST_1}: {samples.VALUE_1_LST_1},{samples.LABEL_2_LST_1}: {samples.VALUE_2_LST_1}",
            f"{samples.LABEL_1_LST_2}: {samples.VALUE_1_LST_2},{samples.LABEL_2_LST_2}: {samples.VALUE_2_LST_2}",
        ]
        self.assertEqual(expected_result, [item.listing_data for item in result])

    def test_get_add_listing(self):
        with self.app.test_request_context(method=web_models.RESTMethod.GET):
            request_data = {
                "site": SSLV_NAME,
                "url": samples.SSLV_URL_1,
            }
            request_form = ImmutableMultiDict(request_data)
            result = self.ads_manager.get_add_listing(request_form)
            self.assertIsInstance(result, NewURLForm)

    def test_add_listing(self):
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {
                "site": SSLV_NAME,
                "url": samples.SSLV_URL_1,
            }
            request_form = ImmutableMultiDict(request_data)
            result1, result2 = self.ads_manager.add_listing(request_form)
            self.assertIsInstance(result1, NewURLForm)
            self.assertIsInstance(result2.options1, choices.SelectField)
            self.assertIsInstance(result2.field1, simple.StringField)
            self.assertIsInstance(result2.submit, simple.SubmitField)
            self.assertIsInstance(result2.url, simple.StringField)
            self.assertIsInstance(result2.site, choices.SelectField)
            self.assertIsInstance(result2.email, simple.StringField)

    def test_add_listing_invalid_url(self):
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {"site": SSLV_NAME, "url": "test"}
            request_form = ImmutableMultiDict(request_data)

            with self.assertRaises(ValidationError):
                self.ads_manager.add_listing(request_form)

    def test_add_listing_no_filters_found(self):
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {
                "site": SSLV_NAME,
                "url": samples.SSLV_URL_2,
            }
            request_form = ImmutableMultiDict(request_data)

            with self.assertRaises(NoFiltersFound):
                self.ads_manager.add_listing(request_form)

    def test_save_listing(self):
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {
                "site": SSLV_NAME,
                "url": samples.SSLV_URL_1,
                "email": samples.EMAIL,
                "options1": samples.OPTION_1,
            }
            request_form = ImmutableMultiDict(request_data)
            self.ads_manager.save_listing(request_form)

    def test_save_listing_missing_email(self):
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {"site": SSLV_NAME, "url": samples.SSLV_URL_1}
            request_form = ImmutableMultiDict(request_data)

            with self.assertRaises(ValidationError):
                self.ads_manager.save_listing(request_form)

    def test_save_listing_limit_reached(self):
        self.fake_current_user.user_id = samples.ITEM_ID_2
        with self.app.test_request_context(method=web_models.RESTMethod.POST):
            request_data = {
                "site": SSLV_NAME,
                "url": samples.SSLV_URL_1,
                "email": samples.EMAIL,
                "options1": samples.OPTION_1,
            }
            request_form = ImmutableMultiDict(request_data)

            with self.assertRaises(LimitReached):
                self.ads_manager.save_listing(request_form)

    def test_delete_listing(self):
        result = self.ads_manager.delete_listing(samples.ITEM_ID_1)
        self.assertIsNone(result)

    def test_delete_listing_no_listing_found(self):
        result = self.ads_manager.delete_listing(samples.ITEM_ID_2)
        self.assertIsNone(result)
