from unittest import mock

from flask_login import LoginManager
from flask_testing import TestCase

from src import db, create_app
from src.ads_manager import LimitReached, ValidationError
from src.forms.custom_listings import NoFiltersFound
from src.blueprints.views import COULD_NOT_GET_FILTERS
from src.ds.models import User
from src.forms.general_forms import NewURLForm, NewListing
from src.models import web_models
from src.models.general_models import FlaskSetting
from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME
from test import samples

MODULE_PATH = "src.blueprints.views"
CLIENT_CONFIG = {"follow_redirects": True}

login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class ViewsTest(TestCase):
    @mock.patch("src.DB_NAME", ":memory:")
    def create_app(self):
        app = create_app()
        app.config[FlaskSetting.TESTING] = True
        app.config[FlaskSetting.WTF_CSRF_ENABLED] = False
        app.config[FlaskSetting.LOGIN_DISABLED] = True

        return app

    def setUp(self):
        db.create_all()
        self.patch_ads_manager()
        self.authenticated_client()

    def tearDown(self):
        self.ads_manager.reset_mock()
        db.session.remove()
        db.drop_all()

    def patch_ads_manager(self):
        self.ads_manager = mock.MagicMock()
        self.ads_manager.get_user_listings.return_value = []
        patcher = mock.patch(MODULE_PATH + ".ads_manager", self.ads_manager)
        patcher.start()

    def test_my_listings(self):
        response = self.client.post(web_models.WebPath.root.endpoint, **CLIENT_CONFIG)
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def authenticated_client(self):
        login_manager.init_app(self.app)
        login_manager.user_loader(load_user)
        with self.client:
            yield self.client

    def test_post_add(self):
        new_url_form = NewURLForm(site=SSLV_NAME, url=samples.SSLV_URL_1)
        new_listing_form = NewListing(
            site=SSLV_NAME, url=samples.SSLV_URL_1, email=samples.EMAIL
        )
        self.ads_manager.add_listing.return_value = (new_url_form, new_listing_form)
        response = self.client.post(
            web_models.WebPath.add.endpoint,
            data={**samples.ADD_SSLV_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_post_add_no_filters_found(self):
        self.ads_manager.add_listing.side_effect = NoFiltersFound
        response = self.client.post(
            web_models.WebPath.add.endpoint,
            data={**samples.ADD_SSLV_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
        self.assertIn(COULD_NOT_GET_FILTERS.encode(), response.data)

    def test_post_add_invalid_url(self):
        self.ads_manager.add_listing.side_effect = ValidationError(
            "error", errors={"test1": "test value 1", "test2": "test value 2 "}
        )
        form_payload = {**samples.ADD_SSLV_FORM_DATA}
        form_payload["url"] = "test"
        response = self.client.post(
            web_models.WebPath.add.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_get_add(self):
        response = self.client.get(
            web_models.WebPath.add.endpoint,
            data={**samples.ADD_SSLV_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_post_save_listing(self):
        response = self.client.post(
            web_models.WebPath.save_listing.endpoint,
            data={**samples.SAVE_SSLV_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_post_save_listing_limit_reached(self):
        self.ads_manager.save_listing.side_effect = LimitReached
        response = self.client.post(
            web_models.WebPath.save_listing.endpoint,
            data={**samples.SAVE_SSLV_FORM_DATA},
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_post_save_listing_validation_error(self):
        self.ads_manager.save_listing.side_effect = ValidationError(
            "error", errors={"test1": "test value 1", "test2": "test value 2 "}
        )
        form_payload = {**samples.SAVE_SSLV_FORM_DATA}
        form_payload["url"] = "https://google.com"
        form_payload["url"] = "example.com"
        response = self.client.post(
            web_models.WebPath.save_listing.endpoint, data=form_payload, **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)

    def test_post_delete_listing(self):
        request_data = {"listingID": samples.ITEM_ID_1}
        response = self.client.post(
            web_models.WebPath.delete_listing.endpoint,
            json=request_data,
            **CLIENT_CONFIG
        )
        self.assertEqual(response.status_code, web_models.HTTPResponseCode.OK)
