from unittest import mock, TestCase

from src.ds.models import UserListings, ListingAds
from src.models.general_models import Ad
from src.tasks.ad_checker import Notifier, EMAIL_SUBJECT, EMAIL_AD_ITEM
from test import samples


class TestNotifier(TestCase):
    def setUp(self):
        self.patch_listing_ads()
        self.patch_scraper_factory()
        self.mailer = mock.MagicMock()
        self.notifier = Notifier(
            listing_ads=self.listing_ads,
            mailer=self.mailer,
            scraper_factory=self.scraper_factory,
        )

    def patch_listing_ads(self):
        self.listing_ads = mock.MagicMock()
        self.listing_ads_obj = ListingAds(ad_id=samples.ITEM_ID_1)
        self.user_listings_obj = UserListings(id=samples.ITEM_ID_1, email=samples.EMAIL)
        self.listing_ads.get_all_user_listings.return_value = [self.user_listings_obj]
        self.listing_ads.find_listing_by_id.return_value = [self.listing_ads_obj]

    def patch_scraper_factory(self):
        self.scraper = mock.MagicMock()
        self.ad = Ad(
            id=samples.ITEM_ID_2,
            data={},
            title=samples.TITLE_1,
            link=samples.SSLV_URL_1,
        )
        self.scraper.get_latest_ads.return_value = [self.ad]
        self.scraper_factory = mock.MagicMock()
        self.scraper_factory.get_scraper.return_value = self.scraper

    def test_check_ads(self):
        self.notifier.check_ads()
        self.listing_ads.delete_ads_by_listing_id.assert_called_once_with(
            samples.ITEM_ID_1
        )
        self.listing_ads.add_new_ad.assert_called_once_with(
            listing_id=samples.ITEM_ID_1, ad=self.ad
        )
        self.mailer.send_mail.assert_called_once_with(
            receiver_email=samples.EMAIL,
            message=(
                EMAIL_SUBJECT
                + EMAIL_AD_ITEM.format(title=samples.TITLE_1, link=samples.SSLV_URL_1)
            ).encode(),
        )

    def test_check_ads_nothing_new(self):
        self.ad.id = samples.ITEM_ID_1
        self.notifier.check_ads()
        self.listing_ads.delete_ads_by_listing_id.assert_not_called()
        self.listing_ads.add_new_ad.assert_not_called()
        self.mailer.send_mail.assert_not_called()
