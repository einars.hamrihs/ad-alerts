from unittest import mock, TestCase

from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME
from src.scrapers.scrapers import ScraperFactory, NoScraperRegistered
from test import samples
from test.test_data import sslv_sample_results

sample_html = open("test/test_data/sslv_test_data.html", "r").read()
sample_filter_html = open("test/test_data/sslv_filter_test_data.html", "r").read()
MODULE_PATH = "src.scrapers.adapters.sslv"


class TestSSlv(TestCase):
    def setUp(self):
        self.scraper_factory = ScraperFactory()
        self.sslv_scraper = self.scraper_factory.get_scraper(
            SSLV_NAME, samples.SSLV_REAL_URL
        )

    def test_get_get_scraper_non_existing(self):
        with self.assertRaises(NoScraperRegistered):
            self.scraper_factory.get_scraper(
                "non-existing", "https://nonexistingsite.com"
            )

    @mock.patch(MODULE_PATH + ".requests.post")
    def test_get_ad_list(self, mock_post):
        mock_post.return_value = mock.MagicMock(content=sample_html)

        result = self.sslv_scraper.get_latest_ads(
            sslv_sample_results.sample_filters_input
        )
        self.assertEqual(result, sslv_sample_results.latest_ads_expected_result)

    @mock.patch(MODULE_PATH + ".requests.get")
    def test_parse_filters(self, mock_post):
        mock_post.return_value = mock.MagicMock(content=sample_filter_html)

        filters = self.sslv_scraper.get_filters()
        self.assertEqual(sslv_sample_results.get_filters_expected_result, filters)
