from src.scrapers.ports.scraper import ScraperInterface
from typing import List
from test import samples


class FakeScraper(ScraperInterface):
    def __init__(self, url=samples.SSLV_URL_1):
        self.url = url

    def _get_filters(self) -> List:
        filters = {
            samples.SSLV_URL_1: [
                {
                    "options_list": samples.OPTIONS_LIST,
                    "name": samples.OPTIONS_NAME,
                    "label": samples.OPTIONS_LABEL,
                },
                {
                    "options_list": None,
                    "name": samples.FIELD_NAME,
                    "label": samples.OPTIONS_LABEL,
                },
            ]
        }
        return filters.get(self.url)

    def _get_latest_ads(self, filters: List) -> List:
        ads = {
            samples.SSLV_URL_1: [
                {
                    "id": samples.ITEM_ID_1,
                    "title": samples.TITLE_1,
                    "link": samples.SSLV_URL_1,
                },
                {
                    "id": samples.ITEM_ID_2,
                    "title": samples.TITLE_1,
                    "link": samples.SSLV_URL_2,
                },
            ]
        }
        return ads.get(self.url)


class FakeScraperFactory:
    @staticmethod
    def get_scraper(site: str, url: str):
        return FakeScraper(url)
