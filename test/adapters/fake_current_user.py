import mock

from src.ds.ports.current_user import CurrentUserInterface
from test import samples
from src.ds.models import UserListings


class FakeCurrentUser(CurrentUserInterface):
    def __init__(self, user_id=1):
        self.user_id = user_id

    def _delete_user(self):
        return

    def _get_id(self):
        return self.user_id

    def _get_user_obj(self):
        return mock.MagicMock()

    def _get_email(self):
        return samples.EMAIL

    def _get_password(self):
        return samples.SHA256_PASSWORD

    def _change_password(self, new_password: str):
        return

    def _change_email(self, new_email: str):
        return

    def _get_listings(self):
        return [
            UserListings(listing_data=samples.LISTING_DATA_1),
            UserListings(listing_data=samples.LISTING_DATA_2),
        ]
