from src.ds.ports.users import UsersInterface
from src.ds.models import User
from unittest import mock
from src.forms.general_forms import SignUpForm
from test import samples


class FakeUsersAdapter(UsersInterface):
    def _get_first_user_by_email(self, email: str) -> User:
        users = {
            samples.EMAIL: mock.MagicMock(),
        }
        return users.get(email)

    def _save_new_user(self, user: SignUpForm) -> User:
        return mock.MagicMock()
