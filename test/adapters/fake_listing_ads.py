from src.ds.ports.listing_ads import ListingAdsInterface
from typing import List


class FakeListingAdsAdapter(ListingAdsInterface):
    def _get_all_user_listings(self) -> List:
        return []

    def _find_listing_by_id(self, listing_id: int) -> List:
        return []

    def _delete_ads_by_listing_id(self, listing_id: int) -> None:
        return

    def _add_new_ad(self, listing_id: int, ad: dict) -> None:
        return
