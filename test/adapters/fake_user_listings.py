import mock

from src.ds.ports.user_listings import UserListingsInterface
from typing import List
from test import samples


class FakeUserListingsAdapter(UserListingsInterface):
    def _get_all_user_listings(self):
        return []

    def _get_user_listing_by_listing_id(self, listing_id: int):
        user_listings = {
            samples.ITEM_ID_1: mock.MagicMock(user_id=samples.ITEM_ID_1),
            samples.ITEM_ID_2: None,
        }
        return user_listings[listing_id]

    def _get_user_listings_count(self, user_id: int) -> int:
        user_listings_count = {samples.ITEM_ID_1: 3, samples.ITEM_ID_2: 5}
        return user_listings_count[user_id]

    def _add_new_user_listing(
        self, email: str, site: str, url: str, listing_data: List, user_id: int
    ) -> int:
        return samples.ITEM_ID_1

    def _delete_user_listing(self, listing) -> None:
        return

    def _get_user_listing_by_user_id(self, user_id) -> List:
        return [mock.MagicMock(id=samples.ITEM_ID_1)]

    def _delete_user_listings_by_user_id(self, user_id: int) -> None:
        return
