from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME


EMAIL = "test@example.com"
EMAIL_2 = "test2@example.com"
PASSWORD = "testpassword"
NEW_PASSWORD = "newpassword"
SHA256_PASSWORD = "sha256$qqYMRjnRsPJspQk2$e5a6faa20529bff095a9796b4f1da1ead48c74bcb6dd278458e3027993a4b34a"
SSLV_URL_1 = "https://ss.lv/12345"
SSLV_URL_2 = "https://ss.lv/23456"
SSLV_REAL_URL = "https://www.ss.lv/lv/real-estate/flats/riga/centre/fDgSeF4S.html"
LABEL_1_LST_1 = "label 1 lst 1"
LABEL_2_LST_1 = "label 2 lst 1"
VALUE_1_LST_1 = "value 1 lst 1"
VALUE_2_LST_1 = "value 2 lst 1"
LABEL_1_LST_2 = "label 1 lst 2"
LABEL_2_LST_2 = "label 2 lst 2"
VALUE_1_LST_2 = "label 2 lst 2"
VALUE_2_LST_2 = "value 2 lst 2"
LISTING_DATA_1 = [
    {"label": LABEL_1_LST_1, "value": VALUE_1_LST_1},
    {"label": LABEL_2_LST_1, "value": VALUE_2_LST_1},
]
LISTING_DATA_2 = [
    {"label": LABEL_1_LST_2, "value": VALUE_1_LST_2},
    {"label": LABEL_2_LST_2, "value": VALUE_2_LST_2},
]
OPTION_1 = "option1"
OPTIONS_LIST = [OPTION_1, "option2", "option3"]
OPTIONS_NAME = "options1"
OPTIONS_LABEL = "options 1"
FIELD_NAME = "field1"
TITLE_1 = "test 1"
TITLE_2 = "test 2"
ITEM_ID_1 = 1
ITEM_ID_2 = 2
LOGIN_FORM_DATA = {"email": EMAIL, "password": PASSWORD}
SIGNUP_FORM_DATA = {"email": EMAIL_2, "password1": PASSWORD, "password2": PASSWORD}
CHANGE_PASS_FORM_DATA = {
    "old_password": PASSWORD,
    "password1": NEW_PASSWORD,
    "password2": NEW_PASSWORD,
}
ADD_SSLV_FORM_DATA = {"site": SSLV_NAME, "url": SSLV_URL_1}
SAVE_SSLV_FORM_DATA = {**ADD_SSLV_FORM_DATA, "email": EMAIL}
