from __future__ import annotations

from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, validators, SubmitField, PasswordField
from wtforms.validators import ValidationError, DataRequired, Email, Length
from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME

from src.ds.ports.users import UsersInterface

EMAIL_EXISTS = "Email already exists."
INVALID_EMAIL = "Invalid email address."
EMAIL_MISSING = "Email missing!"
INVALID_URL = "Invalid URL"
PASSWORDS_MUST_MATCH = "Passwords must match"


class SignUpForm(FlaskForm):
    class Meta:
        csrf = False

    def __init__(self, users_ds: UsersInterface, *args, **kwargs):
        self.user_ds = users_ds
        super().__init__(*args, **kwargs)

    email = StringField(
        label=("Email"),
        validators=[
            DataRequired(message=EMAIL_MISSING),
            Email(),
            Length(max=120, message=INVALID_EMAIL),
        ],
    )
    password1 = PasswordField(
        "Password",
        [
            validators.DataRequired(),
            validators.EqualTo("password2", message=PASSWORDS_MUST_MATCH),
            validators.Length(min=8, max=35),
        ],
    )
    password2 = PasswordField("Password")

    def validate_email(self, field):
        user = self.user_ds.get_first_user_by_email(field.data)
        if user:
            raise ValidationError(EMAIL_EXISTS)


class UpdateProfileForm(FlaskForm):
    class Meta:
        csrf = False

    def __init__(self, users_ds: UsersInterface, *args, **kwargs):
        self.user_ds = users_ds
        super().__init__(*args, **kwargs)

    email = StringField(
        label=("Email"),
        validators=[
            DataRequired(message=EMAIL_MISSING),
            Email(),
            Length(max=120, message=INVALID_EMAIL),
        ],
    )
    password = PasswordField(
        "Password",
        [
            validators.DataRequired(),
        ],
    )

    def validate_email(self, field):
        user = self.user_ds.get_first_user_by_email(field.data)
        if user:
            raise ValidationError(EMAIL_EXISTS)


class ChangePasswordForm(FlaskForm):
    class Meta:
        csrf = False

    old_password = PasswordField("Old password")
    password1 = PasswordField(
        "Password 1",
        [
            validators.DataRequired(),
            validators.EqualTo("password2", message=PASSWORDS_MUST_MATCH),
            validators.Length(min=8, max=35),
        ],
    )
    password2 = PasswordField("Password 2")


class NewURLForm(FlaskForm):
    site = SelectField(
        "Site",
        choices=[(SSLV_NAME, "ss.lv/ss.com")],
        validators=[validators.input_required()],
    )
    url = StringField("URL", validators=[validators.input_required()])

    def validate_url(self, field):
        valid_urls = [
            "https://ss.com/",
            "https://ss.lv/",
            "https://www.ss.com/",
            "https://www.ss.lv/",
        ]
        for url in valid_urls:
            f = field.data
            if f.startswith(url):
                return
        raise ValidationError(INVALID_URL)

    submit = SubmitField(label=("Load"))


class NewListing(FlaskForm):
    url = StringField(
        "URL", render_kw={"readonly": True}, validators=[validators.input_required()]
    )
    site = SelectField(
        "Site",
        render_kw={"readonly": True},
        choices=[(SSLV_NAME, "ss.lv/ss.com")],
        validators=[validators.input_required()],
    )
    email = StringField(
        label=("Email"),
        validators=[
            DataRequired(message=EMAIL_MISSING),
            Email(),
            Length(max=120, message=INVALID_EMAIL),
        ],
    )
