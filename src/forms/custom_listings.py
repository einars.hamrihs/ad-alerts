from typing import Type, Optional

from wtforms import StringField, SelectField, SubmitField

from src.forms.general_forms import NewListing
from src.scrapers.ports.scraper import ScraperInterface
from src.scrapers.scrapers import ScraperFactory
from pydantic import BaseModel


class NoFiltersFound(Exception):
    pass


class ListingParam(BaseModel):
    label: str
    name: str
    value: Optional[str] = None


class NewCustomListing(NewListing):
    @property
    def listing_params(self) -> list[ListingParam]:
        params = []
        for item in self:
            params.append(
                ListingParam(label=item.description, name=item.name, value=item.data)
            )
        return params

    @property
    def listing_params_serialized(self) -> list[dict]:
        return [item.dict() for item in self.listing_params]


def create_options_field(filter_item: dict) -> SelectField:
    return SelectField(
        filter_item["label"],
        choices=filter_item["options_list"],
        description=filter_item["label"],
    )


def create_string_field(filter_item: dict) -> StringField:
    return StringField(filter_item["label"], description=filter_item["label"])


def add_filter_attr(cls: Type[NewCustomListing], filter_item: dict):
    if filter_item["options_list"]:
        field = create_options_field(filter_item)
    else:
        field = create_string_field(filter_item)
    setattr(
        cls,
        filter_item["name"],
        field,
    )


def add_filters_to_listing_class(
    scraper: ScraperInterface, cls: Type[NewCustomListing]
):
    filters = scraper.get_filters()
    if not filters:
        raise NoFiltersFound()
    for item in filters:
        add_filter_attr(cls=cls, filter_item=item)


def create_custom_listing(
    scraper_factory: ScraperFactory, site: str, url: str
) -> Type[NewCustomListing]:
    class NewCustomListingImpl(NewCustomListing):
        pass

    scraper = scraper_factory.get_scraper(site=site, url=url)
    add_filters_to_listing_class(scraper=scraper, cls=NewCustomListingImpl)
    return NewCustomListingImpl


def create_custom_listing_with_submit(
    scraper_factory: ScraperFactory, site: str, url: str
) -> Type[NewCustomListing]:
    listing_cl = create_custom_listing(
        scraper_factory=scraper_factory, site=site, url=url
    )
    setattr(listing_cl, "submit", SubmitField(label=("Submit")))
    return listing_cl
