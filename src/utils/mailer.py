import logging
import smtplib
import ssl

from src.settings import Settings

settings = Settings()


class Mailer:
    log = logging.getLogger("src.sub")

    def __init__(self):
        self.smtp_server = settings.smtp_server
        self.port = settings.smtp_port
        self.smtp_username = settings.smtp_username
        self.sender_email = settings.smtp_sender
        self.password = settings.smtp_password
        self.context = ssl.create_default_context()

    def send_mail(self, receiver_email: str, message: bytes) -> None:
        server = smtplib.SMTP(self.smtp_server, self.port)
        try:
            server.ehlo()
            server.starttls(context=self.context)
            server.ehlo()
            server.login(self.smtp_username, self.password)
            server.sendmail(self.sender_email, receiver_email, message)
        except Exception as e:
            self.log.error(e)
        finally:
            server.quit()
            return
