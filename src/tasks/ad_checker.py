from __future__ import annotations

import logging
from typing import TYPE_CHECKING

from extensions import celery
from src.ds.adapters.listing_ads_sql_alchemy import ListingAdsAdapter
from src.ds.models import UserListings
from src.models import general_models
from src.scrapers.scrapers import ScraperFactory
from src.utils.mailer import Mailer

if TYPE_CHECKING:
    from src.ds.ports.listing_ads import ListingAdsInterface


EMAIL_SUBJECT = "Subject: Ievietoti jauni sludinājumi\n\n"
EMAIL_AD_ITEM = "{title}\n• URL: {link}\n\n"


class Notifier:
    log = logging.getLogger("src.sub")

    def __init__(
        self,
        listing_ads: ListingAdsInterface,
        mailer: Mailer,
        scraper_factory: ScraperFactory,
    ):
        self.listing_ads = listing_ads
        self.mailer = mailer
        self.scraper_factory = scraper_factory

    def get_old_ad_ids(self, listing: UserListings) -> list[str]:
        old_ads = self.listing_ads.find_listing_by_id(listing.id)
        return [ad.ad_id for ad in old_ads]

    def get_latest_ads(self, listing: UserListings) -> list[general_models.Ad]:
        scraper = self.scraper_factory.get_scraper(listing.site, listing.url)
        return scraper.get_latest_ads(listing.listing_data)

    @staticmethod
    def get_latest_ad_ids(ads: list[general_models.Ad]) -> list[str]:
        return [ad.id for ad in ads]

    def get_unprocessed_ads(
        self, listing: UserListings
    ) -> tuple[list[str], list[general_models.Ad]]:
        old_ad_ids = self.get_old_ad_ids(listing)
        latest_ads = self.get_latest_ads(listing)
        latest_ad_ids = self.get_latest_ad_ids(latest_ads)
        return list(set(latest_ad_ids) - set(old_ad_ids)), latest_ads

    def add_new_ads_to_db(
        self, listing: UserListings, latest_ads: list[general_models.Ad]
    ) -> None:
        for ad in latest_ads:
            self.listing_ads.add_new_ad(listing_id=listing.id, ad=ad)

    @staticmethod
    def create_email_content(
        latest_ads: list[general_models.Ad], ad_diff: list[str]
    ) -> str:
        content = EMAIL_SUBJECT
        for ad in latest_ads:
            if ad.id in ad_diff:
                content += EMAIL_AD_ITEM.format(title=ad.title, link=ad.link)
        return content

    def check_listing_for_new_ads(self, listing: UserListings) -> None:
        ad_diff, latest_ads = self.get_unprocessed_ads(listing)
        if not ad_diff:
            return

        self.log.info(f"Found new ads for user {listing.email}")
        self.listing_ads.delete_ads_by_listing_id(listing.id)

        self.add_new_ads_to_db(listing=listing, latest_ads=latest_ads)
        email_content = self.create_email_content(
            latest_ads=latest_ads, ad_diff=ad_diff
        )
        self.mailer.send_mail(
            receiver_email=listing.email,
            message=email_content.encode(general_models.Encoding.utf_8),
        )

    def check_ads(self) -> None:
        self.log.info("Checking new ads...")
        listings = self.listing_ads.get_all_user_listings()
        for listing in listings:
            self.check_listing_for_new_ads(listing)


listings_ads = ListingAdsAdapter()
mailer = Mailer()
scraper_factory = ScraperFactory()
notifier = Notifier(
    listing_ads=listings_ads, mailer=mailer, scraper_factory=scraper_factory
)


@celery.task()
def check_ads() -> None:
    notifier.check_ads()
