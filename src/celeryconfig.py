from celery.schedules import crontab
from src.settings import Settings

settings = Settings()

imports = "src.tasks.ad_checker"
task_result_expires = 30
timezone = settings.celery_timezone

accept_content = ["json", "msgpack", "yaml"]
task_serializer = "json"
result_serializer = "json"

beat_schedule = {
    "check_ads": {
        "task": "src.tasks.ad_checker.check_ads",
        # Every minute
        "schedule": crontab(minute="*/5"),
    }
}
