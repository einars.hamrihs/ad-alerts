from __future__ import annotations

from typing import List, TYPE_CHECKING, Optional
from src.models import general_models, scraper_models

import requests
from bs4 import BeautifulSoup

from src.scrapers.ports.scraper import ScraperInterface

if TYPE_CHECKING:
    from bs4 import element


SCRAPER_NAME = "SSlv"


class SSlvFilterScraper:
    def __init__(self, url: str):
        self.url = url
        self.soup_obj = None
        self.create_soup()

    def create_soup(self):
        base_page = requests.get(self.url)
        self.soup_obj = BeautifulSoup(base_page.content, "html.parser")

    @staticmethod
    def get_field_name(td: element.Tag) -> Optional[str]:
        if td.input:
            return td.find(scraper_models.HTMLTag.input)["name"]
        elif td.select:
            return td.find(scraper_models.HTMLTag.select)["name"]

    @staticmethod
    def extract_option(opt: element.Tag) -> tuple[str, str]:
        opt_label = opt.decode_contents()
        opt_val = opt["value"]
        return opt_val, opt_label

    def parse_options_field(self, td: element.Tag) -> List:
        options_list = []
        options = td.findAll(scraper_models.HTMLTag.option)
        for opt in options[: len(options) // 2]:
            options_list.append(self.extract_option(opt))
        return options_list

    @staticmethod
    def add_min_max_filter(
        filters: List, label: str, name: str, options_list: List
    ) -> None:
        filters.append(
            scraper_models.SSlvFilter(
                label=f"{label} min", name=f"{name}[min]", options_list=options_list
            ).dict()
        )
        filters.append(
            scraper_models.SSlvFilter(
                label=f"{label} max", name=f"{name}[max]", options_list=options_list
            ).dict()
        )

    @staticmethod
    def add_general_filter(
        filters: List, label: str, name: str, options_list: List
    ) -> None:
        filters.append(
            scraper_models.SSlvFilter(
                label=label, name=name, options_list=options_list
            ).dict()
        )

    def scrape_filters(self) -> list:
        filters_html = self.soup_obj.findAll(
            scraper_models.HTMLTag.td, class_="filter_name"
        )
        filters = []
        for td in filters_html:
            td_content = td.decode_contents().split(":")
            label = td_content[0]
            name = self.get_field_name(td)
            options_list = self.parse_options_field(td)

            if name[-5:] == "[min]":
                self.add_min_max_filter(filters, label, name[0:-5], options_list)
            else:
                self.add_general_filter(filters, label, name, options_list)

        return filters


class SSlvAdsScraper:
    def __init__(self, url: str):
        self.url = url
        self.cookies = self.get_cookies(self.url)

    @staticmethod
    def get_cookies(url: str) -> dict:
        a_session = requests.Session()
        a_session.get(url)
        return a_session.cookies.get_dict()

    @staticmethod
    def get_filters(filters: list) -> dict:
        return {item["name"]: item["value"] for item in filters}

    @staticmethod
    def get_data_titles(soup: BeautifulSoup) -> list[str]:
        data_titles = []
        data_title_html = soup.findAll(
            scraper_models.HTMLTag.td, {"class": ["msg_column_td", "msg_column"]}
        )

        for item in data_title_html:
            title = item.find(scraper_models.HTMLTag.a).decode_contents()
            if title != "datums":
                data_titles.append(title)
        return data_titles

    def get_soup(self, params: dict) -> BeautifulSoup:
        result = requests.post(self.url, cookies=self.cookies, data=params)
        return BeautifulSoup(result.content, "html.parser")

    @staticmethod
    def filter_ads(soup: BeautifulSoup) -> element.ResultSet:
        return soup.findAll(
            scraper_models.HTMLTag.tr, {"id": lambda r: r and r.startswith("tr_")}
        )

    def get_ad_list(self, filters: List) -> list[general_models.Ad]:
        params = self.get_filters(filters)
        soup = self.get_soup(params)
        data_titles = self.get_data_titles(soup)
        html_ads = self.filter_ads(soup)
        ad_list = []

        for item in html_ads:
            ad = general_models.Ad.create_from_html(tag=item, data_titles=data_titles)
            if ad.data:
                ad_list.append(ad)

        return ad_list


class SSlvScraper(ScraperInterface):
    def __init__(self, url: str):
        self.url = self.parse_url(url)
        self.ads_scraper = SSlvAdsScraper(self.url)
        self.filter_scraper = SSlvFilterScraper(self.url)

    def _get_latest_ads(self, filters: List) -> list[general_models.Ad]:
        return self.ads_scraper.get_ad_list(filters)

    def _get_filters(self) -> List:
        return self.filter_scraper.scrape_filters()

    @staticmethod
    def parse_url(url: str) -> str:
        url = url[: url.rindex("/")]
        if not url.endswith("filter"):
            url += "/filter/"
        return url
