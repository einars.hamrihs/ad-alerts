from src.scrapers.adapters.sslv import SSlvScraper
from typing import Type
from src.scrapers.ports.scraper import ScraperInterface
from src.scrapers.adapters.sslv import SCRAPER_NAME as SSLV_NAME


class NoScraperRegistered(Exception):
    pass


class ScraperFactory:
    def __init__(self):
        self.scrapers = {}
        self.register_scrapers()

    def get_scraper(self, site: str, url: str) -> ScraperInterface:
        scraper_cl = self.scrapers.get(site)
        if not scraper_cl:
            raise NoScraperRegistered("No scraper found for requested site.")
        return self.scrapers[site](url)

    def register_scraper(self, name: str, scraper_cl: Type[ScraperInterface]) -> None:
        self.scrapers[name] = scraper_cl

    def register_scrapers(self):
        self.register_scraper(SSLV_NAME, SSlvScraper)
