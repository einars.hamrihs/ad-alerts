from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from src.models import general_models


class ScraperInterface(ABC):
    def get_latest_ads(self, filters: List) -> list[general_models.Ad]:
        return self._get_latest_ads(filters)

    def get_filters(self) -> List:
        return self._get_filters()

    @abstractmethod
    def _get_latest_ads(self, filters: List) -> list[general_models.Ad]:
        raise NotImplementedError

    @abstractmethod
    def _get_filters(self) -> List:
        raise NotImplementedError
