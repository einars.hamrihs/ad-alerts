import os

from dotenv import load_dotenv
from pydantic import BaseSettings, Field

project_folder = os.path.expanduser("~/ad-alerts")
env_path = os.path.join(project_folder, ".env")
if os.path.exists(env_path):
    load_dotenv(env_path, verbose=True)
else:
    load_dotenv(verbose=True)


class Settings(BaseSettings):
    # default values to None for unit tests
    flask_key: str = Field(None, env="FLASK_KEY")
    db_name: str = Field(None, env="SQLALCHEMY_DATABASE_URI")
    celery_timezone: str = Field(None, env="CELERY_TIMEZONE")
    smtp_server: str = Field(None, env="SMTP_SERVER")
    smtp_port: int = Field(None, env="SMTP_PORT")
    smtp_username: str = Field(None, env="SMTP_USERNAME")
    smtp_sender: str = Field(None, env="SMTP_SENDER")
    smtp_password: str = Field(None, env="SMTP_PASSWORD")
