from enum import Enum


class Path(Enum):
    @property
    def endpoint(self):
        return f"/{self.value}"

    @property
    def html(self):
        return f"{self.name}.html"


class WebPath(Path):
    root = ""
    delete_profile = "delete-profile"
    edit_profile = "edit-profile"
    sign_up = "sign-up"
    logout = "logout"
    login = "login"
    add = "add"
    save_listing = "save-listing"
    delete_listing = "delete-listing"
    my_listings = "my_listings"


class HTTPResponseCode:
    OK = 200
    REDIRECT = 302


class RESTMethod:
    POST = "POST"
    GET = "GET"
    DELETE = "DELETE"


class FlashCategory:
    SUCCESS = "success"
    ERROR = "error"


class Message:
    SUCCESS = "Success"
    ERROR = "Error"
