from pydantic import BaseModel


class SSlvFilter(BaseModel):
    label: str
    name: str
    options_list: list


class HTMLTag:
    input = "input"
    select = "select"
    option = "option"
    td = "td"
    tr = "tr"
    a = "a"
