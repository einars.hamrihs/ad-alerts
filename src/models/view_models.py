from enum import Enum


class BaseView(Enum):
    @property
    def object_type(self):
        return getattr(self, "_object_type")

    @property
    def view(self) -> str:
        return f"{self.object_type.value}.{self.value}"


class Auth(BaseView):
    _object_type = "auth"
    login = "login"


class Views(BaseView):
    _object_type = "views"
    my_listings = "my_listings"
