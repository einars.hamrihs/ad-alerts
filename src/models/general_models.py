from dataclasses import dataclass
from bs4.element import Tag


class FlaskSetting:
    TESTING = "TESTING"
    WTF_CSRF_ENABLED = "WTF_CSRF_ENABLED"
    WTF_CSRF_METHODS = "WTF_CSRF_METHODS"
    LOGIN_DISABLED = "LOGIN_DISABLED"


@dataclass
class Ad:
    id: str
    data: dict
    title: str = ""
    link: str = ""

    @staticmethod
    def get_link(msg2: list[Tag]) -> str:
        return "https://ss.com" + msg2[0].find("a")["href"] if msg2 else ""

    @staticmethod
    def get_title(msg2: list[Tag]) -> str:
        if not msg2:
            return ""
        title = msg2[0].find("a", class_="am").decode_contents()
        for tag in ["<b>", "</b>"]:
            title = title.replace(tag, "")
        return title

    @staticmethod
    def get_ad_data(data_titles: list, msga2_o: list[Tag]) -> dict:
        ad_data_list = []
        for data_item in msga2_o:
            field_data = data_item.decode_contents()
            for tag in ["<b>", "</b>"]:
                field_data = field_data.replace(tag, "")
            ad_data_list.append(field_data)
        return dict(zip(data_titles, ad_data_list))

    @classmethod
    def create_from_html(cls, tag: Tag, data_titles: list[str]) -> "Ad":
        msg2 = tag.findAll("td", class_="msg2")
        msga2_o = tag.findAll("td", class_="msga2-o")

        return cls(
            id=tag["id"],
            title=cls.get_title(msg2),
            link=cls.get_link(msg2),
            data=cls.get_ad_data(data_titles, msga2_o),
        )


class Encoding:
    utf_8 = "utf-8"


class HashAlgorithm:
    SHA256 = "sha256"
