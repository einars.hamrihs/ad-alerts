import logging
from os import path

from celery import Celery
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

from src import celeryconfig
from src.settings import Settings

db = SQLAlchemy()
settings = Settings()
DB_NAME = settings.db_name


def create_app():
    app = Flask(__name__)
    app.config["SECRET_KEY"] = settings.flask_key
    app.config["SQLALCHEMY_DATABASE_URI"] = f"sqlite:///{DB_NAME}"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True

    app.config["CELERY_BROKER_URL"] = f"sqla+sqlite:///src/{DB_NAME}"
    app.config["result_backend"] = f"db+sqlite:///src/{DB_NAME}"

    app.logger.setLevel(logging.DEBUG)

    db.init_app(app)

    from src.blueprints.views import views
    from src.blueprints.auth import auth

    app.register_blueprint(views, url_prefix="/")
    app.register_blueprint(auth, url_prefix="/")

    from src.ds.models import User, UserListings, ListingAds

    create_database(app)

    login_manager = LoginManager()
    login_manager.login_view = "auth.login"
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    return app


def create_database(app):
    if not path.exists("src/" + DB_NAME):
        db.create_all(app=app)


def make_celery(app):
    celery = Celery(
        app.import_name,
        backend=f"sqla+sqlite:///src/{DB_NAME}",
        broker=f"sqla+sqlite:///src/{DB_NAME}",
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    celery.config_from_object(celeryconfig)
    return celery
