from __future__ import annotations

from src.ds.ports.listing_ads import ListingAdsInterface
from src.ds.models import UserListings, ListingAds
from src import db
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.models import general_models


class ListingAdsAdapter(ListingAdsInterface):
    def _get_all_user_listings(self) -> list[UserListings]:
        return UserListings.query.all()

    def _find_listing_by_id(self, listing_id: int) -> list[ListingAds]:
        return ListingAds.query.filter_by(listing_id=listing_id).all()

    def _delete_ads_by_listing_id(self, listing_id: int) -> ListingAds:
        return ListingAds.query.filter_by(listing_id=listing_id).delete()

    def _add_new_ad(self, listing_id: int, ad: general_models.Ad) -> None:
        ad_obj = ListingAds(
            ad_id=ad.id,
            title=ad.title,
            link=ad.link,
            listing_id=listing_id,
        )
        db.session.add(ad_obj)
        db.session.commit()
