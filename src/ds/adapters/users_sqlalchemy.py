from __future__ import annotations

from typing import TYPE_CHECKING

from werkzeug.security import generate_password_hash

from src import db
from src.ds.models import User
from src.ds.ports.users import UsersInterface
from src.models import general_models

if TYPE_CHECKING:
    from src.forms.general_forms import SignUpForm


class UsersAdapter(UsersInterface):
    def _get_first_user_by_email(self, email: str) -> User:
        return User.query.filter_by(email=email).first()

    def _save_new_user(self, user: SignUpForm) -> User:
        new_user = User(
            email=user.email.data,
            password=generate_password_hash(
                user.password1.data, method=general_models.HashAlgorithm.SHA256
            ),
        )
        db.session.add(new_user)
        db.session.commit()
        return new_user
