from __future__ import annotations

from typing import TYPE_CHECKING

from flask_login import current_user

from src import db
from src.ds.ports.current_user import CurrentUserInterface, UserObj

if TYPE_CHECKING:
    from src.ds.models import UserListings


class CurrentUserAdapter(CurrentUserInterface):
    def _delete_user(self) -> None:
        db.session.delete(current_user)
        db.session.commit()

    def _get_id(self) -> str:
        return current_user.id

    def _get_user_obj(self) -> UserObj:
        return current_user

    def _get_email(self) -> str:
        return current_user.email

    def _get_password(self) -> str:
        return current_user.password

    def _change_password(self, new_password: str) -> None:
        current_user.password = new_password

    def _change_email(self, new_email: str) -> None:
        current_user.email = new_email

    def _get_listings(self) -> list[UserListings]:
        return current_user.listings
