from src import db
from src.ds.models import UserListings, ListingAds
from src.ds.ports.user_listings import UserListingsInterface


class UserListingsAdapter(UserListingsInterface):
    def _get_all_user_listings(self) -> list[UserListings]:
        return UserListings.query.all()

    def _get_user_listing_by_listing_id(self, listing_id: int) -> list[UserListings]:
        return UserListings.query.get(listing_id)

    def _get_user_listing_by_user_id(self, user_id: int) -> list[UserListings]:
        return UserListings.query.filter_by(user_id=user_id)

    def _get_user_listings_count(self, user_id: str) -> int:
        return UserListings.query.filter_by(user_id=user_id).count()

    def _add_new_user_listing(
        self, email: str, site: str, url: str, listing_data: list[dict], user_id: int
    ) -> int:
        new_db_listing = UserListings(
            email=email,
            site=site,
            url=url,
            listing_data=listing_data,
            user_id=user_id,
        )
        db.session.add(new_db_listing)
        db.session.commit()
        return new_db_listing.id

    def _delete_user_listing(self, listing) -> None:
        db.session.delete(listing)
        db.session.commit()

    def _delete_user_listings_by_user_id(self, user_id: int) -> None:
        UserListings.query.filter_by(user_id=user_id).delete()
        db.session.commit()
