from flask_login import UserMixin
from sqlalchemy.sql import func

from src import db


class UserListings(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    site = db.Column(db.String(100))
    url = db.Column(db.String(100))
    listing_data = db.Column(db.JSON)
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))

    @staticmethod
    def create_filter_str(filter_data: dict) -> str:
        return "{label}: {value}".format(
            label=str(filter_data["label"]), value=str(filter_data.get("value"))
        )

    def serialize_listing_data(self):
        filter_list = []
        for filter_data in self.listing_data:
            if filter_data.get("label") and filter_data.get("value"):
                filter_list.append(self.create_filter_str(filter_data))
        self.listing_data = ",".join(filter_list)


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(150), unique=True)
    password = db.Column(db.String(150))
    listings = db.relationship("UserListings")


class ListingAds(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    ad_id = db.Column(db.String(50))
    title = db.Column(db.String(100))
    link = db.Column(db.String(100))
    date = db.Column(db.DateTime(timezone=True), default=func.now())
    listing_id = db.Column(db.Integer, db.ForeignKey("user_listings.id"))
