from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.ds.models import UserListings


class UserObj:
    @property
    @abstractmethod
    def is_authenticated(self):
        raise NotImplementedError

    @property
    @abstractmethod
    def email(self):
        raise NotImplementedError


class CurrentUserInterface(ABC):
    def delete_user(self) -> None:
        return self._delete_user()

    def get_id(self) -> str:
        return self._get_id()

    def get_user_obj(self) -> UserObj:
        return self._get_user_obj()

    def get_email(self) -> str:
        return self._get_email()

    def get_password(self) -> str:
        return self._get_password()

    def change_password(self, new_password: str) -> None:
        return self._change_password(new_password)

    def change_email(self, new_email: str) -> None:
        return self._change_email(new_email)

    def get_listings(self) -> list[UserListings]:
        return self._get_listings()

    @abstractmethod
    def _delete_user(self) -> None:
        raise NotImplementedError

    @abstractmethod
    def _get_id(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def _get_user_obj(self) -> UserObj:
        raise NotImplementedError

    @abstractmethod
    def _get_email(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def _get_password(self) -> str:
        raise NotImplementedError

    @abstractmethod
    def _change_password(self, new_password: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def _change_email(self, new_email: str) -> None:
        raise NotImplementedError

    @abstractmethod
    def _get_listings(self) -> list[UserListings]:
        raise NotImplementedError
