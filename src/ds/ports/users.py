from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.ds.models import User
    from src.forms.general_forms import SignUpForm


class UsersInterface(ABC):
    def get_first_user_by_email(self, email: str) -> User:
        return self._get_first_user_by_email(email)

    def save_new_user(self, user: SignUpForm) -> User:
        return self._save_new_user(user)

    @abstractmethod
    def _get_first_user_by_email(self, email: str) -> User:
        raise NotImplementedError

    def _save_new_user(self, user: SignUpForm) -> User:
        raise NotImplementedError
