from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.models import general_models
    from src.ds.models import UserListings


class ListingAdsInterface(ABC):
    def get_all_user_listings(self) -> list[UserListings]:
        return self._get_all_user_listings()

    def find_listing_by_id(self, listing_id: int) -> list:
        return self._find_listing_by_id(listing_id)

    def delete_ads_by_listing_id(self, listing_id: int) -> list:
        return self._delete_ads_by_listing_id(listing_id)

    def add_new_ad(self, listing_id: int, ad: general_models.Ad) -> list:
        return self._add_new_ad(listing_id, ad)

    @abstractmethod
    def _get_all_user_listings(self) -> list[UserListings]:
        raise NotImplementedError

    @abstractmethod
    def _find_listing_by_id(self, listing_id: int) -> list:
        raise NotImplementedError

    @abstractmethod
    def _delete_ads_by_listing_id(self, listing_id: int) -> list:
        raise NotImplementedError

    @abstractmethod
    def _add_new_ad(self, listing_id: int, ad: general_models.Ad) -> list:
        raise NotImplementedError
