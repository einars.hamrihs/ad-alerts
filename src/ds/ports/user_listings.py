from __future__ import annotations

from abc import ABC, abstractmethod
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from src.ds.models import UserListings


class UserListingsInterface(ABC):
    def get_user_listings_count(self, user_id: str) -> int:
        return self._get_user_listings_count(user_id)

    def get_user_listing_by_listing_id(self, listing_id: int):
        return self._get_user_listing_by_listing_id(listing_id)

    def get_all_user_listings(self) -> list[UserListings]:
        return self._get_all_user_listings()

    def add_new_user_listing(
        self, email: str, site: str, url: str, listing_data: list[dict], user_id: str
    ) -> int:
        return self._add_new_user_listing(email, site, url, listing_data, user_id)

    def delete_user_listing(self, listing) -> None:
        return self._delete_user_listing(listing)

    def get_user_listing_by_user_id(self, user_id: str) -> list[UserListings]:
        return self._get_user_listing_by_user_id(user_id)

    def delete_user_listings_by_user_id(self, user_id: str) -> None:
        return self._delete_user_listings_by_user_id(user_id)

    @abstractmethod
    def _get_user_listings_count(self, user_id: str) -> int:
        raise NotImplementedError

    @abstractmethod
    def _get_user_listing_by_listing_id(self, listing_id: int):
        raise NotImplementedError

    @abstractmethod
    def _get_all_user_listings(self) -> list[UserListings]:
        raise NotImplementedError

    @abstractmethod
    def _add_new_user_listing(
        self, email: str, site: str, url: str, listing_data: list[dict], user_id: str
    ) -> int:
        raise NotImplementedError

    @abstractmethod
    def _delete_user_listing(self, listing) -> None:
        raise NotImplementedError

    @abstractmethod
    def _get_user_listing_by_user_id(self, user_id: str) -> list[UserListings]:
        raise NotImplementedError

    @abstractmethod
    def _delete_user_listings_by_user_id(self, user_id: str) -> None:
        raise NotImplementedError
