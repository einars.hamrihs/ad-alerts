from __future__ import annotations

from typing import TYPE_CHECKING
from typing import Tuple

from src.forms.custom_listings import (
    create_custom_listing,
    create_custom_listing_with_submit,
    NewCustomListing,
)
from src.forms.general_forms import NewURLForm, NewListing

if TYPE_CHECKING:
    from src.ds.ports.user_listings import UserListingsInterface
    from src.ds.ports.listing_ads import ListingAdsInterface
    from src.ds.ports.current_user import CurrentUserInterface
    from werkzeug.datastructures import ImmutableMultiDict


class LimitReached(Exception):
    pass


MAX_LISTINGS_PER_USER = 4


class ValidationError(Exception):
    def __init__(self, message, errors):
        super().__init__(message)
        self.errors = errors


class AdsManager:
    def __init__(
        self,
        current_user_adapter: CurrentUserInterface,
        scraper_factory,
        user_listings_adapter: UserListingsInterface,
        listing_ads_adapter: ListingAdsInterface,
    ):
        self.current_user = current_user_adapter
        self.scraper_factory = scraper_factory
        self.user_listings_adapter = user_listings_adapter
        self.listing_ads_adapter = listing_ads_adapter

    def get_user_listings(self) -> list:
        user_listings = self.current_user.get_listings()

        for item in user_listings:
            item.serialize_listing_data()

        return user_listings

    @staticmethod
    def get_add_listing(form_data: ImmutableMultiDict) -> NewURLForm:
        return NewURLForm(form_data)

    def add_listing(
        self, form_data: ImmutableMultiDict
    ) -> Tuple[NewURLForm, NewListing]:
        new_url_form = NewURLForm(form_data)
        if not new_url_form.validate_on_submit():
            raise ValidationError("Validation error", errors=new_url_form.errors)

        listing_cl = create_custom_listing_with_submit(
            scraper_factory=self.scraper_factory,
            site=new_url_form.site.data,
            url=new_url_form.url.data,
        )
        new_listing = listing_cl(form_data)

        return new_url_form, new_listing

    def validate_listing_count(self):
        rows_count = self.user_listings_adapter.get_user_listings_count(
            self.current_user.get_id()
        )
        if rows_count > MAX_LISTINGS_PER_USER:
            raise LimitReached()

    def validate_new_listing(self, new_listing: NewCustomListing) -> None:
        if not new_listing.validate_on_submit():
            raise ValidationError("Validation error", errors=new_listing.errors)
        self.validate_listing_count()

    def add_ads_to_new_listing(self, new_listing: NewCustomListing, listing_id: int):
        scraper = self.scraper_factory.get_scraper(
            new_listing.site.data, new_listing.url.data
        )
        latest_ads = scraper.get_latest_ads(new_listing.listing_params)
        for ad in latest_ads:
            self.listing_ads_adapter.add_new_ad(listing_id, ad)

    def save_listing(self, form_data: ImmutableMultiDict) -> None:
        listing_cl = create_custom_listing(
            scraper_factory=self.scraper_factory,
            site=form_data.get("site"),
            url=form_data.get("url"),
        )

        new_listing = listing_cl(form_data)
        self.validate_new_listing(new_listing)

        new_listing_id = self.user_listings_adapter.add_new_user_listing(
            email=new_listing.email.data,
            site=new_listing.site.data,
            url=new_listing.url.data,
            listing_data=new_listing.listing_params_serialized,
            user_id=self.current_user.get_id(),
        )
        self.add_ads_to_new_listing(new_listing=new_listing, listing_id=new_listing_id)

    def delete_listing(self, listing_id: int) -> None:
        listing = self.user_listings_adapter.get_user_listing_by_listing_id(listing_id)
        if not listing or listing.user_id != self.current_user.get_id():
            return
        self.listing_ads_adapter.delete_ads_by_listing_id(listing_id)
        self.user_listings_adapter.delete_user_listing(listing)
