import json

from flask import Blueprint, render_template, request, flash, redirect
from flask_login import login_required

from src.ads_manager import (
    AdsManager,
    LimitReached,
    ValidationError,
)
from src.ads_manager import MAX_LISTINGS_PER_USER
from src.ds.adapters.current_user_flask import CurrentUserAdapter
from src.ds.adapters.listing_ads_sql_alchemy import ListingAdsAdapter
from src.ds.adapters.user_listings_sql_alchemy import UserListingsAdapter
from src.forms.custom_listings import NoFiltersFound
from src.models import web_models, field_models
from src.scrapers.scrapers import ScraperFactory

views = Blueprint("views", __name__)


scraper_factory = ScraperFactory()
user_listings_adapter = UserListingsAdapter()
listing_ads_adapter = ListingAdsAdapter()
current_user_adapter = CurrentUserAdapter()

COULD_NOT_GET_FILTERS = "Could not get any filters from url"
MAX_LIMIT_REACHED = f"Max limit of {MAX_LISTINGS_PER_USER} listings reached"

ads_manager = AdsManager(
    current_user_adapter=current_user_adapter,
    scraper_factory=scraper_factory,
    user_listings_adapter=user_listings_adapter,
    listing_ads_adapter=listing_ads_adapter,
)


@views.route(
    web_models.WebPath.root.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
@login_required
def my_listings():
    user_listings = ads_manager.get_user_listings()
    return render_template(
        web_models.WebPath.my_listings.html,
        user=current_user_adapter.get_user_obj(),
        user_listings=user_listings,
    )


@views.route(
    web_models.WebPath.add.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
@login_required
def add():
    def get_add():
        new_url_form = ads_manager.get_add_listing(request.form)
        return render_template(
            web_models.WebPath.add.html,
            user=current_user_adapter.get_user_obj(),
            new_url_form=new_url_form,
        )

    def post_add():
        try:
            new_url_form, new_listing = ads_manager.add_listing(request.form)
            flash(web_models.Message.SUCCESS, category=web_models.FlashCategory.SUCCESS)
            return render_template(
                web_models.WebPath.add.html,
                user=current_user_adapter.get_user_obj(),
                new_url_form=new_url_form,
                new_listing=new_listing,
            )
        except NoFiltersFound:
            flash(COULD_NOT_GET_FILTERS, category=web_models.FlashCategory.ERROR)
            return get_add()
        except ValidationError as e:
            for item in e.errors.values():
                flash(", ".join(item), category=web_models.FlashCategory.ERROR)
            return get_add()

    action_map = {
        web_models.RESTMethod.POST: post_add,
        web_models.RESTMethod.GET: get_add,
    }
    return action_map.get(request.method)()


@views.route(
    web_models.WebPath.save_listing.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
@login_required
def save_listing():
    try:
        ads_manager.save_listing(request.form)
        flash(web_models.Message.SUCCESS, category=web_models.FlashCategory.SUCCESS)
        return redirect(
            web_models.WebPath.root.endpoint, code=web_models.HTTPResponseCode.REDIRECT
        )
    except LimitReached:
        flash(MAX_LIMIT_REACHED, category=web_models.FlashCategory.ERROR)
        return redirect(
            web_models.WebPath.root.endpoint, code=web_models.HTTPResponseCode.REDIRECT
        )
    except ValidationError as e:
        for item in e.errors.values():
            flash(", ".join(item), category=web_models.FlashCategory.ERROR)
        return redirect(
            web_models.WebPath.root.endpoint, code=web_models.HTTPResponseCode.REDIRECT
        )


@views.route(
    web_models.WebPath.delete_listing.endpoint, methods=[web_models.RESTMethod.POST]
)
def delete_listing():
    listing_dict = json.loads(request.data)
    listing_id = listing_dict[field_models.Listing.LISTING_ID]
    ads_manager.delete_listing(listing_id)
    return redirect(
        web_models.WebPath.root.endpoint, code=web_models.HTTPResponseCode.REDIRECT
    )
