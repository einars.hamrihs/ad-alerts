from __future__ import annotations

from typing import TYPE_CHECKING, Union

from flask import Blueprint, render_template, request, flash, redirect, url_for
from flask_login import login_user, login_required, logout_user
from werkzeug.security import generate_password_hash, check_password_hash

from src import db
from src.ds.adapters.current_user_flask import CurrentUserAdapter
from src.ds.adapters.listing_ads_sql_alchemy import ListingAdsAdapter
from src.ds.adapters.user_listings_sql_alchemy import UserListingsAdapter
from src.ds.adapters.users_sqlalchemy import UsersAdapter
from src.forms.general_forms import SignUpForm, UpdateProfileForm, ChangePasswordForm
from src.models import web_models, view_models, field_models, general_models

if TYPE_CHECKING:
    from werkzeug.wrappers.response import Response


auth = Blueprint("auth", __name__)


users = UsersAdapter()
user_listings = UserListingsAdapter()
listing_ads = ListingAdsAdapter()
current_user = CurrentUserAdapter()

INCORRECT_PASSWORD = "Incorrect password."
EMAIL_DOES_NOT_EXIST = "Email does not exist."
LOGGED_IN = "Logged in successfully!"
ACCOUNT_CREATED = "Account created!"
ACCOUNT_CHANGES_SAVED = "Account changes saved!"


@auth.route(
    web_models.WebPath.login.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
def login() -> Union[Response, str]:
    def post_login() -> Union[Response, str]:
        email = request.form.get(field_models.ProfileForm.email)
        password = request.form.get(field_models.ProfileForm.password)
        user = users.get_first_user_by_email(email)
        if not user:
            flash(EMAIL_DOES_NOT_EXIST, category=web_models.FlashCategory.ERROR)
            return get_login()
        if not check_password_hash(user.password, password):
            flash(INCORRECT_PASSWORD, category=web_models.FlashCategory.ERROR)
            return get_login()

        flash(LOGGED_IN, category=web_models.FlashCategory.SUCCESS)
        login_user(user, remember=True)
        return redirect(url_for(view_models.Views.my_listings.view))

    def get_login() -> str:
        return render_template(
            web_models.WebPath.login.html, user=current_user.get_user_obj()
        )

    action_map = {
        web_models.RESTMethod.POST: post_login,
        web_models.RESTMethod.GET: get_login,
    }
    return action_map.get(request.method)()


@auth.route(web_models.WebPath.logout.endpoint)
@login_required
def logout() -> Response:
    logout_user()
    return redirect(url_for(view_models.Auth.login.view))


@auth.route(
    web_models.WebPath.sign_up.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
def sign_up() -> Union[Response, str]:
    def invalid_sign_up_form(errors: dict) -> str:
        for item in errors.values():
            flash(", ".join(item), category=web_models.FlashCategory.ERROR)
        return get_sign_up()

    def post_sign_up() -> Union[Response, str]:
        user = SignUpForm(
            users_ds=users,
            email=request.form.get(field_models.ProfileForm.email),
            password1=request.form.get(field_models.ProfileForm.password1),
            password2=request.form.get(field_models.ProfileForm.password2),
            csrf_enabled=False,
        )
        if not user.validate_on_submit():
            return invalid_sign_up_form(user.errors)

        new_user = users.save_new_user(user)
        login_user(new_user, remember=True)
        flash(ACCOUNT_CREATED, category=web_models.FlashCategory.SUCCESS)
        return redirect(url_for(view_models.Views.my_listings.view))

    def get_sign_up() -> str:
        return render_template(
            web_models.WebPath.sign_up.html, user=current_user.get_user_obj()
        )

    action_map = {
        web_models.RESTMethod.POST: post_sign_up,
        web_models.RESTMethod.GET: get_sign_up,
    }
    return action_map.get(request.method)()


@auth.route(
    web_models.WebPath.edit_profile.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
def edit_profile() -> Union[Response, str]:
    def invalid_profile_update_form(errors: dict) -> str:
        for item in errors.values():
            flash(", ".join(item), category=web_models.FlashCategory.ERROR)
        return get_edit_profile()

    def passwords_do_not_match() -> str:
        flash(INCORRECT_PASSWORD, category=web_models.FlashCategory.ERROR)
        return get_edit_profile()

    def update_profile() -> Union[Response, str]:
        user = UpdateProfileForm(
            users_ds=users,
            email=request.form.get(field_models.ProfileForm.email),
            password=request.form.get(field_models.ProfileForm.password),
            csrf_enabled=False,
        )
        if not user.validate_on_submit():
            return invalid_profile_update_form(user.errors)

        if not check_password_hash(current_user.get_password(), user.password.data):
            return passwords_do_not_match()

        current_user.change_email(user.email.data)
        db.session.commit()
        flash(ACCOUNT_CHANGES_SAVED, category=web_models.FlashCategory.SUCCESS)
        return redirect(url_for(view_models.Views.my_listings.view))

    def invalid_change_password_form(errors: dict) -> str:
        for item in errors.values():
            flash(", ".join(item), category=web_models.FlashCategory.ERROR)
        return get_edit_profile()

    def change_password() -> Union[Response, str]:
        password_form = ChangePasswordForm(
            old_password=request.form.get(field_models.ProfileForm.old_password),
            password1=request.form.get(field_models.ProfileForm.password1),
            password2=request.form.get(field_models.ProfileForm.password2),
        )
        if not password_form.validate_on_submit():
            return invalid_change_password_form(password_form.errors)

        if not check_password_hash(
            current_user.get_password(), password_form.old_password.data
        ):
            return passwords_do_not_match()

        current_user.change_password(
            generate_password_hash(
                password_form.password1.data, method=general_models.HashAlgorithm.SHA256
            )
        )
        db.session.commit()
        flash(ACCOUNT_CHANGES_SAVED, category=web_models.FlashCategory.SUCCESS)
        return redirect(url_for(view_models.Views.my_listings.view))

    def post_edit_profile() -> Union[Response, str]:
        if request.form.get(field_models.ProfileForm.email):
            return update_profile()
        elif request.form.get(field_models.ProfileForm.old_password):
            return change_password()

    def get_edit_profile() -> str:
        return render_template(
            web_models.WebPath.edit_profile.html, user=current_user.get_user_obj()
        )

    action_map = {
        web_models.RESTMethod.POST: post_edit_profile,
        web_models.RESTMethod.GET: get_edit_profile,
    }
    return action_map.get(request.method)()


@auth.route(
    web_models.WebPath.delete_profile.endpoint,
    methods=[web_models.RESTMethod.GET, web_models.RESTMethod.POST],
)
def delete_profile() -> Union[Response, str]:
    def post_delete_profile() -> Response:
        listings = user_listings.get_user_listing_by_user_id(
            user_id=current_user.get_id()
        )
        for item in listings:
            listing_ads.delete_ads_by_listing_id(item.id)
        user_listings.delete_user_listings_by_user_id(current_user.get_id())
        current_user.delete_user()
        logout_user()
        return redirect(url_for(view_models.Auth.login.view))

    def get_delete_profile() -> str:
        return render_template(
            web_models.WebPath.delete_profile.html, user=current_user.get_user_obj()
        )

    action_map = {
        web_models.RESTMethod.POST: post_delete_profile,
        web_models.RESTMethod.GET: get_delete_profile,
    }
    return action_map.get(request.method)()
