# ad-alerts
A Flask based web service, where registered users can add listings from ad websites (like ss.com) and get email notifications, when new listings are added.


## Getting started

1. Run the website:
```
python3.9 main.py
```
2. Run the celery worker
```
celery -A run-celery worker --loglevel=INFO --pidfile=''
```
3. Run the celery beat
```
celery -A run-celery beat --loglevel=INFO --pidfile=''
```

### Pre-requisites

Create a python virtual environment and install the requirements with following commands in project directory.
> python3.9 -m venv venv\

on Linux:
> source venv/bin/activate

On Windows:
> venv\Scripts\activate.bat

> pip install -r requirements.txt
