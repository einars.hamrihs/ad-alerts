#!/bin/bash

target_dir="/etc/systemd/system/"
USER_HOME=$(eval echo ~${SUDO_USER})

sed -i -e 's:homedir:'"${USER_HOME}"':g' systemd/ad-alerts-website.service
sed -i -e 's:homedir:'"${USER_HOME}"':g' systemd/ad-alerts-worker.service
sed -i -e 's:homedir:'"${USER_HOME}"':g' systemd/ad-alerts-beat.service

cp -u "systemd/ad-alerts-website.service" "$target_dir"
cp -u "systemd/ad-alerts-worker.service" "$target_dir"
cp -u "systemd/ad-alerts-beat.service" "$target_dir"

#systemctl daemon-reload
#systemctl start ad-alerts-website
#systemctl start ad-alerts-worker
#systemctl start ad-alerts-beat
#systemctl enable ad-alerts-website
#systemctl enable ad-alerts-worker
#systemctl enable ad-alerts-beat
