from src import create_app, make_celery

celery_app = create_app()
celery = make_celery(celery_app)
